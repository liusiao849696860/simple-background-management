import request from '@/utils/request'

export function forward (params) {
    return request({
        url: `/micro/requests/download`,
        method: 'get',
        responseType: 'blob',
        params:params
    })
}