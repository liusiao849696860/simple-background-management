import request from '@/utils/request'

export function getBackworkLogs(params) {
  return request({
    url: 'micro/backworker-logs',
    method: 'get',
    params:params
  })
}

export function getBackworkLogById(params) {
  return request({
    url: 'micro/backworker-logs/' + params,
    method: 'get',
  })
}