import request from '@/utils/request'

export function GetFile (fileId) {
    return request({
        url: `micro/files/${fileId}/download`,
        method: 'get',
        responseType: 'blob'
    })
}