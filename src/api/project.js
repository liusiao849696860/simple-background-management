import request from '@/utils/request'

export function getType() {
    return request({
      url: 'micro/projects/type',
      method: 'get'
    })
  }

  
export function getMark() {
    return request({
      url: 'micro/projects/mark',
      method: 'get'
    })
  }

  export function getProjects(params) {
    return request({
      url: 'micro/projects',
      method: 'get',
      params:params
    })
  }
  
  export function getProject(projectId) {
    return request({
      url: 'micro/projects/' + projectId,
      method: 'get'
    })
  }
  
  export function deleteProject(projectId) {
    return request({
      url: 'micro/projects/' + projectId,
      method: 'delete'
    })
  }
  
  export function createProject(data) {
    return request({
      url: 'micro/projects',
      method: 'post',
      data: JSON.stringify(data),
      headers:{
        'Content-Type':'application/json'
      }
    })
  }
  
  export function updateProject(projectId, data) {
    return request({
      url: 'micro/projects/'+ projectId,
      method: 'put',
      data: JSON.stringify(data),
      headers:{
        'Content-Type':'application/json'
      }
    })
  }