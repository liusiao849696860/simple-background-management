import request from '@/utils/request'

export function StatCount(params) {
  return request({
    url: 'micro/stats/count',
    method: 'get',
    params:params
  })
}

export function StatCreatedCount(params) {
  return request({
    url: 'micro/stats/created-count',
    method: 'get',
    params:params
  })
}