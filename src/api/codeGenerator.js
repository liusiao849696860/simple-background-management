import request from '@/utils/request'

export function create(data) {
    return request({
      url: 'micro/code-generators',
      method: 'post',
      data: JSON.stringify(data),
      headers:{
        'Content-Type':'application/json'
      }
    })
  }