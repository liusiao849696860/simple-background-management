import request from '@/utils/request'


  export function getEntityModels(params) {
    return request({
      url: 'micro/entity-models',
      method: 'get',
      params:params
    })
  }
  
  export function getEntityModel(id) {
    return request({
      url: 'micro/entity-models/' + id,
      method: 'get'
    })
  }

  export function preview(id) {
    return request({
      url: 'micro/entity-models/' + id + '/preview',
      method: 'get'
    })
  }
  
  export function deleteEntityModel(id) {
    return request({
      url: 'micro/entity-models/' + id,
      method: 'delete'
    })
  }
  
  export function createEntityModel(data) {
    return request({
      url: 'micro/entity-models',
      method: 'post',
      data: JSON.stringify(data),
      headers:{
        'Content-Type':'application/json'
      }
    })
  }
  
  export function updateEntityModel(id, data) {
    return request({
      url: 'micro/entity-models/'+ id,
      method: 'put',
      data: JSON.stringify(data),
      headers:{
        'Content-Type':'application/json'
      }
    })
  }