import request from '@/utils/request'

export function getImageItems(params) {
  return request({
    url: 'micro/image-items',
    method: 'get',
    params:params
  })
}
