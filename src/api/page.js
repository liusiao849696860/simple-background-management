import request from '@/utils/request'

export function getPages(params) {
    return request({
      url: 'micro/pages',
      method: 'get',
      params:params
    })
  }
  
  
  export function getPageType() {
    return request({
      url: 'micro/pages/type',
      method: 'get'
    })
  }