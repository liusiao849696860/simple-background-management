import request from '@/utils/request'

export function getImageCategory(params) {
  return request({
    url: 'micro/image-categories',
    method: 'get',
    params:params
  })
}

export function getImageCategoryById(params) {
  return request({
    url: 'micro/image-categories/' + params,
    method: 'get',
  })
}