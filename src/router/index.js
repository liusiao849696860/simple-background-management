import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',

    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/admin/index'),
        name: 'Dashboard',
        meta: {
          title: '首页',
          icon: 'dashboard',
          affix: true 
        },
        // meta: { title: 'dashboard', icon: 'dashboard', affix: true }
      }
    ]
  },
  // 素材管理
  {
    path: '/material',
    component: Layout,
    redirect: '/material/upload',
    meta: {
      title: '素材管理',
      icon: 'plane'
    },
    children: [{
        path: 'check-template',
        name: 'check-template',
        component: () => import('@/views/material/check-template'),
        meta: {
          title: '查看模板',
        }
      },
      {
        path: 'logo',
        name: 'logo',
        component: () => import('@/views/material/check-logo'),
        meta: {
          title: '查看logo',
        }
      },
      {
        path: 'generate',
        name: 'generate',
        component: () => import('@/views/material/generate'),
        meta: {
          title: '生成素材',
        }
      },
      {
        path: 'check',
        name: 'check',
        component: () => import('@/views/material/check'),
        meta: {
          title: '查看素材',
        }
      },
    ]
  },
  // 代码生成
  {
    path: '/codeGenerator',
    component: Layout,
    redirect: '/codeGenerator',
    meta: {
      title: '代码生成',
      icon: 'tool'
    },
    children: [
      {
        path: 'entityModel',
        name: 'entityModel',
        component: () => import('@/views/codeGenerator/entityModel'),
        meta: {
          title: '实体模板',
        }
      },
      {
        path: 'codeGenerator',
        name: 'codeGenerator',
        hidden: true,
        component: () => import('@/views/codeGenerator/codeGenerator'),
        meta: {
          title: '代码生成模板',
        }
      }
     
    ]
  },
    // // 系统工具
    // {
    //   path: '/tool',
    //   component: Layout,
    //   redirect: '/tool',
    //   meta: {
    //     title: '系统工具',
    //     icon: 'tool'
    //   },
    //   children: [
    //     {
    //       path: 'swagger',
    //       name: 'swagger',
    //       component: () => import('@/views/swagger/index'),
    //       meta: {
    //         title: '系统接口',
    //       }
    //     }
    //   ]
    // },
  // //测试页面
  // {
  //   path: '/test',
  //   hidden: false,
  //   component: Layout,
  //   meta: {
  //     title: '爬虫',
  //     icon: 'table'
  //   },
  //   children: [{
  //     path: 'project',
  //     name: 'project',
  //     component: () => import('@/views/test/project'),
  //     meta: {
  //       title: '项目',
  //     }
  //   },
  //   {
  //     path: 'page',
  //     name: 'page',
  //     component: () => import('@/views/test/page'),
  //     meta: {
  //       title: '页面',
  //     }
  //   },
  //   {
  //     path: 'imageCategory',
  //     name: 'imageCategory',
  //     component: () => import('@/views/test/imageCategory'),
  //     meta: {
  //       title: '图片目录',
  //     }
  //   },
  //   {
  //     path: 'imageItem',
  //     name: 'imageItem',
  //     component: () => import('@/views/test/imageItem'),
  //     meta: {
  //       title: '图片目录项',
  //     },
  //   },
  //   {
  //     path: 'backworkLogs',
  //     name: 'backworkLogs',
  //     component: () => import('@/views/test/backworkLogs'),
  //     meta: {
  //       title: '后台任务日志',
  //     },
  //   }]
  // },
  // 404 page must be placed at the end !!!
  {
    path: '*',
    redirect: '/404',
    hidden: true
  }
  
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
